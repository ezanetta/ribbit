package com.ezanetta.ribbit.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.ezanetta.ribbit.R;
import com.ezanetta.ribbit.ui.FriendsFragment;
import com.ezanetta.ribbit.ui.InboxFragment;

import java.util.Locale;

/**
 * Created by ezanetta on 5/9/15.
 */
public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

    Context mContext;
    CharSequence mTitles[]; // This will Store the Titles of the Tabs which are Going to be passed when ViewPagerAdapter is created
    int mNumbOfTabs; // Store the number of tabs, this will also be passed when the ViewPagerAdapter is created
    int mIcons[];


    // Build a Constructor and assign the passed Values to appropriate values in the class
    public SectionsPagerAdapter(Context context,FragmentManager fm,CharSequence titles[], int numbOfTabsumb, int icons[]) {
        super(fm);

        mTitles = titles;
        mNumbOfTabs = numbOfTabsumb;
        mIcons = icons;
        mContext = context;

    }


    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        switch(position){
            case 0:
                return new InboxFragment();
            case 1:
                return new FriendsFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Locale l = Locale.getDefault();
        switch (position) {
            case 0:
                return mContext.getString(R.string.title_section1).toUpperCase(l);
            case 1:
                return mContext.getString(R.string.title_section2).toUpperCase(l);
        }
        return null;
    }

    public int getIcon(int position) {
        switch (position) {
            case 0:
                return R.drawable.ic_tab_inbox;
            case 1:
                return R.drawable.ic_tab_friends;
        }
        return R.drawable.ic_tab_inbox;
    }
}
