package com.ezanetta.ribbit;

import android.app.Application;

import com.ezanetta.ribbit.utils.ParseConstants;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.ParseUser;


/**
 * Created by ezanetta on 5/9/15.
 */
public class RibbitApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        // PARSE
        Parse.initialize(this, "UzIemrvUUHgyygpNw52PAqpIjYaDTWIjUFLfRYWJ", "xJRQPnvhHoAwCWnvwJWVsVZx2M01GHjHRn6rkLye");
        ParseInstallation.getCurrentInstallation().saveInBackground();

    }

    public static void updateParseInstallation(ParseUser user){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.put(ParseConstants.KEY_USER_ID, user.getObjectId());
        installation.saveInBackground();
    }
}
